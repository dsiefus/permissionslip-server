# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
cert_pem = File.read("certs/user_cert.pem")
cert = OpenSSL::X509::Certificate.new cert_pem

us = User.find_or_create_by(certificate: cert_pem)

perm = Permission.find_or_create_by(perm: "edit Article")
us.permissions.push(perm) unless us.permissions.include? perm

perm = Permission.find_or_create_by(perm: "create Article")
us.permissions.push(perm) unless us.permissions.include? perm

#-----------------------------------

cert_pem = File.read("certs/user_cert2.pem")
cert = OpenSSL::X509::Certificate.new cert_pem

us = User.find_or_create_by(certificate: cert_pem)

perm = Permission.find_or_create_by(perm: "create Article")
us.permissions.push(perm) unless us.permissions.include? perm

#----------------------------------------

art = Article.new
art.title = "Article title"
art.text = "some text"
art.save