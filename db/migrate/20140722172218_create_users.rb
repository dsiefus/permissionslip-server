class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :session_id
      t.integer :user_id
      t.text :certificate

      t.timestamps
    end
  end
end
