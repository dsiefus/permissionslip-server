class CreateSignedSlips < ActiveRecord::Migration
  def change
    create_table :signed_slips do |t|
      t.string :session_id
      t.text :signed_permission_slip

      t.timestamps
    end
  end
end
