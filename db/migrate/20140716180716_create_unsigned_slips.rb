class CreateUnsignedSlips < ActiveRecord::Migration
  def change
    create_table :unsigned_slips do |t|
      t.string :session_id
      t.text :unsigned_permission_slip

      t.timestamps
    end
  end
end
