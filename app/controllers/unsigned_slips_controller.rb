require 'json'

class UnsignedSlipsController < ApplicationController
  #Shows the unsigned slip of the required session_id
  def get_slip
    @unsigned_slip= UnsignedSlip.find(params[:session_id])
    render text: @unsigned_slip[:unsigned_permission_slip]
    rescue ActiveRecord::RecordNotFound
      render status: 400, plain: "No slip found."      
  end

  #Ensures that there are some permissions and calls to create_slip
  def unauthorized  	
    #If no specific permissions are asked, use some default ones
    session[:permissions] = ["create Article, edit Article"] if session[:permissions].nil?   	
    render plain: "Some error occured" unless create_slip(session.id,session[:permissions])    
  end

private 
#Creates an unsigned permission slip with the passed session id and permissions
def create_slip(session_id, permissions)
  #permissions is an array []
  unsigned_slip=UnsignedSlip.where(session_id: session_id).first_or_initialize  	  		
  slip ={}
  slip[:permissions] = permissions
  slip[:expiration_date] = Time.now + 60*60*24 #1 Day default expiration time
  unsigned_slip[:unsigned_permission_slip] = slip.to_json
  unsigned_slip.save    	
end

end
