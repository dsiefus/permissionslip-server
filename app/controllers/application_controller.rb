class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  #If access denied the user is showed the URL with the proposed permission slip
  rescue_from CanCan::AccessDenied do |exception|
    session[:permissions] = [ exception.message]
    redirect_to "/unauthorized"
  end

	def current_signed_slip		
    @current_signed_slip ||= SignedSlip.find_by_session_id(session.id)
  end

  	alias_method :current_user, :current_signed_slip
end
