class ArticlesController < ApplicationController

def new
  @article = Article.new	 
  authorize! :create, @article, message: "create Article"
end

def index	
  authorize! :read, Article, message: "read Article"	 
  @articles = Article.accessible_by(current_ability)	 
 
end

def create
  @article = Article.new(article_params)
  authorize! :create, @article, message: "create Article"
  if @article.save
    redirect_to @article
  else
    render 'new'
  end
end

def show
  @article = Article.find(params[:id])
  authorize! :read, @article, message: "read Article" 
end

def edit
  @article = Article.find(params[:id])
  authorize! :edit, @article, message: "edit Article"
end

def update
  @article = Article.find(params[:id])
  authorize! :edit, @article, message: "edit Article"
  if @article.update(article_params)
    redirect_to @article
  else
    render 'edit'
  end
end

def destroy
  @article = Article.find(params[:id])
  authorize! :destroy, @article, message: "destroy Article"
  @article.destroy 
  redirect_to articles_path
end

private
  def article_params
    params.require(:article).permit(:title, :text)
  end
end
