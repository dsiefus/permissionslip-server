class CommentsController < ApplicationController
  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.create(comment_params)
    authorize! :create, @comment, message: "create Comment"
    redirect_to article_path(@article)
  end

  def destroy
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    authorize! :destroy, @comment, message: "destroy Comment"
    @comment.destroy    
    redirect_to article_path(@article)
  end

  
  private
    def comment_params
      params.require(:comment).permit(:commenter, :body)
    end
end