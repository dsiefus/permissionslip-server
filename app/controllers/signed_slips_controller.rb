require 'json'

class SignedSlipsController < ApplicationController
  #It's necessary to disable forgery protection for save_slip, as it receives a 
  #POST message without auntehnticity token (this is intended)
  protect_from_forgery except: :save_slip

  def save_slip    
    temp = SignedSlip.new(signed_slip_params)    
    temp[:session_id]=params[:session_id]
    @signed_slip = SignedSlip.find_by_session_id(params[:session_id])

    #if there's no slip for this session
    if @signed_slip.nil?
      @signed_slip = temp
    else #if there was a previous slip, update slip (session_id and id not changed)
      @signed_slip[:signed_permission_slip] = temp[:signed_permission_slip]     
    end

    #ensure it's json format
    @signed_slip[:signed_permission_slip] = @signed_slip[:signed_permission_slip].to_json
   
    #this will save or update, depending if the slip existed for this session
    if @signed_slip.save      
      render status: 201, text: "Correctly saved"
    else  		
      errors = ""

      @signed_slip.errors.full_messages.each do |msg| 
        errors+= msg
      end      
      render status: 400,  text: errors
    end    
    
    #Delete all expired slips, should maybe done in another part
    SignedSlip.all.each do |slip| slip.destroy if JSON.parse(slip[:signed_permission_slip])["slip"]["expiration_date"] < Time.now end

  end

private
  def signed_slip_params
    params.require(:signed_slip).permit(:session_id, {signed_permission_slip: [ {slip: [ {permissions: []}, :expiration_date]}, :certificate, :signature]})
 end
  
end
