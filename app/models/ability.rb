class Ability
  include CanCan::Ability

  #Uses the alias current_signed_slip instead of the default current_user  
  def initialize(signed_slip)  
    can :read, [Article, Comment]
    unless signed_slip.nil? || signed_slip.signed_permission_slip.nil?
      slip = JSON.parse(signed_slip.signed_permission_slip)
      if slip["slip"]["expiration_date"] > Time.now
        permissions = slip["slip"]["permissions"]            
        
        can :manage, Article if permissions.include? "manage Article"          
        # can :edit, Article, :id => 3 if permissions.include? "read Article"
        can :create, Article if permissions.include? "create Article"
        can :edit, Article if permissions.include? "edit Article"            
        can :destroy, Article if permissions.include? "destroy Article"              
      
        can :manage, Comment if permissions.include? "manage Comment"          
        can :create, Comment if permissions.include? "create Comment"            
        can :destroy, Comment if permissions.include? "destroy Comment"            
      end
   end
  end
end
