class Permission < ActiveRecord::Base
	has_and_belongs_to_many :users, -> { uniq}
	before_destroy {users.clear}
	validates :perm, presence: true, uniqueness: true
end
