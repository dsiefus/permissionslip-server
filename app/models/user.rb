class UserValidator < ActiveModel::Validator
  def validate(record) 
    unless record.certificate.nil?
      check_certificate(record) 
      #automatic set of user_id based on certificate subject
      record.user_id = (OpenSSL::X509::Certificate.new record.certificate).subject.hash unless record.errors.any?
    end
  end

  def check_certificate(record)
    cert = OpenSSL::X509::Certificate.new record.certificate
    if File.file?('certs/ca_cert.pem')
      ca_cert = OpenSSL::X509::Certificate.new File.read 'certs/ca_cert.pem'
      record.errors.add(:base, "1: User certificate could't be verified. ") unless cert.verify ca_cert.public_key      
      #Checks that the cert is not expired      
      record.errors.add(:base, "2: Expired certificate") unless cert.not_after > Time.now 
    else
      record.errors.add(:base, "3: CA certificate not found. ")
    end
    rescue OpenSSL::X509::CertificateError => e
      record.errors.add(:base, "4: Can't read certificate: "+e.to_s)
  end

end



class User < ActiveRecord::Base
  validates_with UserValidator
  has_and_belongs_to_many :permissions, -> { uniq}
  before_destroy {permissions.clear}
  self.primary_key=:user_id
  validates :user_id, :certificate, presence: true
  validates :user_id, uniqueness: true
end
