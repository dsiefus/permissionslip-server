class SignedSlipValidator < ActiveModel::Validator
  def validate(record)    		
    unless record.signed_permission_slip.nil?   
        signed_hash_variable, cert = basic_format_checking(record)
        unless record.errors.any?          
          check_certificate(signed_hash_variable,cert,record)	
          check_expiration_date(signed_hash_variable, record) 
          check_permissionslip_signature(signed_hash_variable,cert,record)	          	
          check_user_and_permissions_database(signed_hash_variable, cert,record)
        end    
    end
  end

  #Checks that all the required fields exist and that have correct formatting
  def basic_format_checking(record)    
    signed_hash_variable = JSON.parse(record.signed_permission_slip)
    if signed_hash_variable["certificate"].nil?
      record.errors.add(:base, "1: Certificate not found in slip, bad JSON formatting. ")
    else
      cert = OpenSSL::X509::Certificate.new(signed_hash_variable["certificate"])
    end

    if signed_hash_variable["slip"].nil? or 
       signed_hash_variable["slip"]["permissions"].nil? or
       signed_hash_variable["slip"]["expiration_date"].nil? or
       signed_hash_variable["signature"].nil?
         record.errors.add(:base, "2: Incorrect slip formatting, some field missing. ") 
    else      
      record.errors.add(:base, "3: Permissions field must be an array. ") unless signed_hash_variable["slip"]["permissions"].is_a? Array
      t = Time.parse(signed_hash_variable["slip"]["expiration_date"])
    end
    return signed_hash_variable, cert

    rescue JSON::ParserError
      record.errors.add(:base, "4: Bad JSON formatting. ")
    rescue OpenSSL::X509::CertificateError => e
      record.errors.add(:base, "5: Can't read certificate from slip: "+e.to_s)
    rescue ArgumentError #thrown by Time.parse
      record.errors.add(:base, "6: Invalid date format. ")
   
  end

  def check_certificate(signed_hash_variable, cert,record)
    #Checks that the ca certificate exists on the server
    if File.file?('certs/ca_cert.pem')
      ca_cert = OpenSSL::X509::Certificate.new File.read 'certs/ca_cert.pem'
      #Checks that the user cert is issued by the CA
      record.errors.add(:base, "7: User certificate couldn't be verified. ") unless cert.verify ca_cert.public_key      
      #Checks that the cert is not expired      
      record.errors.add(:base, "8: Expired certificate. ") unless cert.not_after > Time.now 
    else
      record.errors.add(:base, "9: CA certificate not found. ")
    end		
    rescue => e
      #Any unexpected error
      record.errors.add(:base, "A: Error reading certificate: "+e.to_s)
  end

  def check_expiration_date(signed_hash_variable,record)  	
    date = signed_hash_variable["slip"]["expiration_date"]
    #Checks that the expiration date has not already passed
    #An option could be to set a minimum/maximum time
    record.errors.add(:base, "B: Expiration date must be in the future. ") if date < Time.now
  end

  #Checks that the signature is valid (slip integrity)
  def check_permissionslip_signature(signed_hash_variable,cert,record)
    digest = OpenSSL::Digest::SHA256.new
    decoded_signature = Base64.decode64(signed_hash_variable["signature"])
    record.errors.add(:base, "C: Incorrect signature. ") unless cert.public_key.verify digest, decoded_signature, signed_hash_variable["slip"].to_json
  end

  def check_user_and_permissions_database(signed_hash_variable,cert, record)  
    #I use the hash of the subject (name) of the certificate as a identifier		
    user_id = cert.subject.hash
    user = User.find(user_id)
    #The certificate sent and saved in DB must be indetical
    #The subject of 2 certificates COULD be the same (leading to same user_id),
    #But that is not acceptable, all the certificate must be the same
    record.errors.add(:base, "D: Certificates do not match. ") unless user.certificate == cert.to_pem

    #Checks that the user is allowed to have all the permissions in the slip
    #Array of permissions of the saved user (on user DB)
    user_permissions = []
    user.permissions.each do |p| user_permissions.push(p.perm) end

    #auxiliary variable to check that at least 1 of the permissions has the correct format
    at_least_one_valid_permissions = false
    #Array of permission on the slip
    slip_permissions = signed_hash_variable["slip"]["permissions"]	
    #For every permission of the slip, it has to appear on the DB    
    slip_permissions.each do |this_perm| 
      #Ignore any permission that is not comprised of 2 words, invalid format
      if this_perm.split.size == 2
        at_least_one_valid_permissions = true
        record.errors.add(:base, "E: Error: user doesn't have permission to "+this_perm+". ") unless (user_permissions.include? this_perm or user_permissions.include? "manage "+this_perm.split()[1])
      end  
    end

    record.errors.add(:base, "G: Permissions format incorrect. Must be of the form 'verb resource'. ") unless at_least_one_valid_permissions

    rescue ActiveRecord::RecordNotFound
      record.errors.add(:base, "F: User/certificate not found. ")
    ensure
      #If everything is correct, we set the user's session_id (not needed for now)
      user.session_id = signed_hash_variable["session_id"] unless record.errors.any? 
  end

end


class SignedSlip < ActiveRecord::Base
  validates_with SignedSlipValidator
  self.primary_key=:session_id
  validates :session_id, :signed_permission_slip, presence: true
  validates :session_id, uniqueness: true
end
