-This proof of concept client is executed running "ruby client.rb".

-First of all it will ask for the session id, which is the pseudo-random string at the end of the URL
 presented when a permission slip is asked.

-It will then retrieve the proposed unsigned slip, and show it. Then a series of questions will be asked to delete
 or add any permission, or to change the expiration date. To answer each question the key "y" or "n" has to be pressed,
 for yes or no.

-It will ask for the digital certificate file, the private key file and the password that protects it. If nothing is
 passed, by default will use the files located in this folder.

-It will then sign and send the permission slip, and print the answer form the server, showing any error if any.