require 'json'
require 'curb'
require 'time'
require 'openssl'
require 'base64'

def yesno
	while true
  begin
    system("stty raw -echo")
    str = STDIN.getc
  ensure
    system("stty -raw echo")
  end

  if str.upcase == "Y"
    return true
  elsif str.upcase == "N" 
    return false
  else
    puts "Please press y (yes) or n (no)"
  end
end
end

def delete_permissions permissions
	puts "For each permission, say if you want to keep it"
	permissions.reject! do |perm| 
		puts "Keep: "+perm+"?"
		!yesno
	end
	return permissions
end


def add_permissions permissions
	while yesno 
		puts "Enter permission to add"	
		input = gets.strip
		permissions.push(input)
		puts "Do you want to add more permissions?"
	end
	return permissions
end

def get_and_edit_permission_slip session_id

	http = Curl.get("http://localhost:3000/permission_slip/"+session_id)

	slip= JSON.parse(http.body_str) 
	slip["expiration_date"]= Time.parse(slip["expiration_date"])
	puts "Permission slip found:"
	puts slip.to_json

	puts "Do you want to delete some of the permissions?"
	slip["permissions"] = delete_permissions slip["permissions"] if yesno

	puts "Do you want to add more permissions?"
	slip["permissions"] = add_permissions slip["permissions"]

	puts "----------------------------"
	puts "FINAL permissions are:"
	puts slip["permissions"]


	puts "----------------------------"
	puts "Do you want to edit the expiration date?: " + slip["expiration_date"].to_s
	if yesno
		puts "How many hours do you want?"
		hours = gets.chomp.to_i
		slip["expiration_date"] = Time.now + hours*60*60
		puts "New expiration date: "+  slip["expiration_date"].to_s
	end

	return slip
end

def sign_and_send_permission_slip unsigned_slip, session_id

	signed_permission_slip = {}
	signed_permission_slip[:slip] = unsigned_slip	

	puts "Enter the certificate file (leave blank for default user_cert.pem)"
	certfile = gets.chomp
	certfile = "user_cert.pem" if certfile.empty?
	signed_permission_slip[:certificate] = File.read(certfile)
	cert = OpenSSL::X509::Certificate.new signed_permission_slip[:certificate]

	puts "Enter the private key file (leave blank for default user_private_key.pem)"
	keyfile = gets.chomp
	keyfile = "user_private_key.pem" if keyfile.empty?
	puts "Enter the password for decrypting the private key (leave blank for 1234)"
	password = gets.chomp
	password = "1234" if password.empty?
	privkey = OpenSSL::PKey::read(File.read(keyfile), password)
	digest = OpenSSL::Digest::SHA256.new

	signature = privkey.sign digest, signed_permission_slip[:slip].to_json
	signed_permission_slip[:signature] = Base64.encode64(signature)
	
	json_hash = {}
	json_hash[:signed_permission_slip] = signed_permission_slip
	json_string = json_hash.to_json()

	
	c = Curl::Easy.http_post("http://localhost:3000/permission_slip/"+session_id,

	     json_string 

	    ) do |curl|
	      curl.headers['Accept'] = 'application/json'
	      curl.headers['Content-Type'] = 'application/json'
	      curl.headers['Api-Version'] = '2.2'
	    end
	    puts c.body_str

	http_response, *http_headers = c.header_str.split(/[\r\n]+/).map(&:strip)
	puts http_response
end


puts "Please enter the session id"
session_id = gets.chomp
unsigned_slip = get_and_edit_permission_slip session_id
sign_and_send_permission_slip unsigned_slip, session_id

