The scripts can be used directly by running "ruby createCA.rb", and then "ruby create_user.rb". 

This will create the files CA_private_key.pem, ca_cert.pem, user1_private_key.pem and user_cert1.pem

The script create_user.rb MUST be changed before creating every new user certificate, by incrementing the "serial" variable
in the top of the script, at least. The password used for encrypting the keys is hardcoded: 31415 for the CA, 1234 
for the user certificates.