require 'openssl'

root_key = OpenSSL::PKey::RSA.new 2048 # the CA's public/private key
#save private key encrypted
password="314159"
cipher = OpenSSL::Cipher.new 'AES-256-CBC'
key_secure = root_key.export cipher, password
open 'CA_private_key.pem', 'w' do |io|
  io.write key_secure
end

root_ca = OpenSSL::X509::Certificate.new
root_ca.version = 2 # cf. RFC 5280 - to make it a "v3" certificate
root_ca.serial = 1
root_ca.subject = OpenSSL::X509::Name.parse "/C=UK/O=Cranfield/CN=Garra-CA"
root_ca.issuer = root_ca.subject # root CA's are "self-signed"
root_ca.public_key = root_key.public_key
root_ca.not_before = Time.now
root_ca.not_after = root_ca.not_before + 2 * 365 * 24 * 60 * 60 # 2 years validity
ef = OpenSSL::X509::ExtensionFactory.new
ef.subject_certificate = root_ca
ef.issuer_certificate = root_ca
root_ca.add_extension(ef.create_extension("basicConstraints","CA:TRUE",true))
root_ca.add_extension(ef.create_extension("keyUsage","keyCertSign, cRLSign", true))
root_ca.add_extension(ef.create_extension("subjectKeyIdentifier","hash",false))
root_ca.add_extension(ef.create_extension("authorityKeyIdentifier","keyid:always",false))
root_ca.sign(root_key, OpenSSL::Digest::SHA256.new)

open("ca_cert.pem", "w") do |io| io.write(root_ca.to_pem) end
