require 'openssl'

serial = 1


# create the end-entity certificate using the root CA certificate.
root_ca = OpenSSL::X509::Certificate.new File.read("ca_cert.pem")
root_key = OpenSSL::PKey::read(File.read("CA_private_key.pem"), "314159")


key = OpenSSL::PKey::RSA.new 2048

#save user's private key encrypted
password_user="1234"
cipher = OpenSSL::Cipher.new 'AES-256-CBC'
key_secure_user = key.export cipher, password_user
open 'user'+serial.to_s+'_private_key.pem', 'w' do |io|
  io.write key_secure_user
end


cert = OpenSSL::X509::Certificate.new
cert.version = 2
cert.serial = serial
cert.subject = OpenSSL::X509::Name.parse "/C=UK/O=Cranfield/CN=cert_"+serial.to_s
cert.issuer = root_ca.subject # root CA is the issuer
cert.public_key = key.public_key
cert.not_before = Time.now
cert.not_after = cert.not_before + 1 * 365 * 24 * 60 * 60 # 1 years validity
ef = OpenSSL::X509::ExtensionFactory.new
ef.subject_certificate = cert
ef.issuer_certificate = root_ca
cert.add_extension(ef.create_extension("keyUsage","digitalSignature", true))
cert.add_extension(ef.create_extension("subjectKeyIdentifier","hash",false))
cert.sign(root_key, OpenSSL::Digest::SHA256.new)

open("user_cert"+serial.to_s+".pem", "w") do |io| io.write(cert.to_pem) end