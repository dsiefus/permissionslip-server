require 'test_helper'

class UnsignedSlipsControllerTest < ActionController::TestCase
  test "should get get_slip with id" do
    get :get_slip, {session_id: 1}
    assert_response 400
    assert_generates '/permission_slip/1', { controller: 'unsigned_slips', action: 'get_slip', session_id: '1' }    
    assert_recognizes({ controller: 'unsigned_slips', action: 'get_slip',  session_id: '1' }, { path: 'permission_slip/1', method: :get })
  end

end
