require 'test_helper'

class SignedSlipsControllerTest < ActionController::TestCase
  #POST with the correct structure of signed_permission_slip so it doesnt give error
  test "correct post bad fields" do
    signed_permission_slip= { slip: { permissions: ["whatever"], expiration_date: "whatever"}, certificate: "ceert", signature: "sig"}
    post :save_slip, {session_id: 1,  signed_slip: signed_permission_slip}   
    assert_response 400
  end

  test "correct post correct fields" do
    cert_pem = File.read("certs/user_cert.pem")
    us = User.find_or_create_by(certificate: cert_pem)
    perm = Permission.find_or_create_by(perm: "read Article")
    us.permissions.push(perm)
    assert us.save

    signed_permission_slip = {}
    signed_permission_slip[:slip] = {}
    signed_permission_slip[:slip][:permissions]=["read Article"]
    signed_permission_slip[:slip][:expiration_date] = (Time.now + 60*60*24).to_json
    signed_permission_slip[:certificate] = cert_pem

    privkey = OpenSSL::PKey::read(File.read("certs/keys/user_private_key.pem"), "1234")
    digest = OpenSSL::Digest::SHA256.new
    signature = privkey.sign digest, signed_permission_slip[:slip].to_json
    signed_permission_slip[:signature] = Base64.encode64(signature)  
    
    signed_slip = {}
    signed_slip[:signed_permission_slip] = signed_permission_slip
    post :save_slip, {session_id: 1,  signed_slip: signed_slip}   
    assert_response 201, @response.body
  end

end
