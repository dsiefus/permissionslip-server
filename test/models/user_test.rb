require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test "empty user creation" do
    u = User.new
    assert_not u.save    
  end

  test "certificate not signed by CA" do
    u = User.new
    cert_pem = File.read("certs/proxy_cert.pem")
    u[:certificate] = cert_pem
    assert_not u.save    
    assert_equal "1", u.errors.full_messages[0][0]
  end

  test "expired certificate" do
    u = User.new
    cert_pem = File.read("certs/expired_cert.pem")
    u[:certificate] = cert_pem
    assert_not u.save
    assert_equal "2", u.errors.full_messages[0][0]
  end

  test "incorrect certificate format" do
    u = User.new
    u[:certificate] = "random string"
    assert_not u.save
    assert_equal "4", u.errors.full_messages[0][0]
  end

  test "duplicate user " do
    u = User.new
    cert_pem = File.read("certs/user_cert.pem")
    u[:certificate] = cert_pem
    assert u.save
    u2 = User.new
    u2[:certificate] = cert_pem
    assert_not u2.save
  end

  test "two users with same id different cert" do
    u = User.new
    cert_pem = File.read("certs/user_cert2.pem")
    u[:certificate] = cert_pem
    assert u.save

    u2 = User.new
    cert_pem_rep = File.read("certs/user_cert2_repeated.pem")
    u2[:certificate] = cert_pem_rep
    assert_not u2.save
  end
    
end
