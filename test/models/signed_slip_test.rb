require 'test_helper'
require 'json'

class SignedSlipTest < ActiveSupport::TestCase

  #some auxiliar methods defined in test/test_helper.rb

  test "empty signed_slip" do    
    
    signed_slip = SignedSlip.new
    assert_not signed_slip.save
    assert_equal 2, signed_slip.errors.count
  end

  test "only session id" do    
    
    signed_slip = SignedSlip.new
    signed_slip.session_id = "any id"
    assert_not signed_slip.save    
    assert_equal 1, signed_slip.errors.count
  end

  test "no certificate no fields" do        
    signed_slip = SignedSlip.new
    signed_slip.session_id = "any id"
    signed_permission_slip = {}
    signed_slip.signed_permission_slip = signed_permission_slip.to_json

    assert_not signed_slip.save    
    assert_operator 2, :<=, signed_slip.errors.count
    assert_equal "1", signed_slip.errors.full_messages[0][0]
    assert_equal "2", signed_slip.errors.full_messages[1][0]
  end

  test "correct cert some field missing" do
    signed_slip, signed_permission_slip = get_slip_with_cert
    signed_slip.signed_permission_slip = signed_permission_slip.to_json

    assert_not signed_slip.save
    assert signed_slip.errors.any? 
    assert_equal "2", signed_slip.errors.full_messages[0][0], signed_slip.errors.full_messages  
  end

  test "bad json format" do
    signed_slip = SignedSlip.new
    signed_slip.session_id = "any id"
    signed_permission_slip = "something that is not correct json format"
    signed_slip.signed_permission_slip = signed_permission_slip

    assert_not signed_slip.save  
    assert signed_slip.errors.any? 
    assert_equal "4", signed_slip.errors.full_messages[0][0]
  end

  test "bad cert format" do    
    signed_slip, signed_permission_slip = get_slip_with_cert

    signed_permission_slip[:certificate] = "random string"
    signed_slip.signed_permission_slip = signed_permission_slip.to_json

    assert_not signed_slip.save    
    assert signed_slip.errors.any? 
    assert_equal "5", signed_slip.errors.full_messages[0][0]    
  end

  test "permissions not an array and incorrect date format" do
    signed_slip, signed_permission_slip = get_slip_with_cert

    slip = {}
    slip[:permissions] = "a string, not an array"
    slip[:expiration_date] = "an incorrectly formatted date"
    signed_permission_slip[:signature] = "an incorrect signature"
    signed_permission_slip[:slip] = slip
    signed_slip.signed_permission_slip = signed_permission_slip.to_json

    assert_not signed_slip.save   
    assert_operator 2, :<=, signed_slip.errors.count
    assert_equal "3", signed_slip.errors.full_messages[0][0]
    assert_equal "6", signed_slip.errors.full_messages[1][0]
  end

  test "user cert not signed by CA" do   
    signed_slip, signed_permission_slip = get_slip_with_cert

    signed_permission_slip = get_full_permission_slip
    signed_permission_slip[:certificate] = File.read("certs/proxy_cert.pem")
    signed_slip.signed_permission_slip = signed_permission_slip.to_json
    
    assert_not signed_slip.save   
    assert signed_slip.errors.any? 
    assert_equal "7", signed_slip.errors.full_messages[0][0]
  end

  test "expired cert" do
    signed_slip, signed_permission_slip = get_slip_with_cert

    signed_permission_slip = get_full_permission_slip
    signed_permission_slip[:certificate] = File.read("certs/expired_cert.pem")
    signed_slip.signed_permission_slip = signed_permission_slip.to_json

    assert_not signed_slip.save
    assert signed_slip.errors.any?
    assert_equal "8", signed_slip.errors.full_messages[0][0]
  end

  test "expired slip" do
    signed_slip, signed_permission_slip = get_slip_with_cert

    signed_permission_slip = get_full_permission_slip
    signed_permission_slip[:slip][:expiration_date] = Time.now - 1
    signed_slip.signed_permission_slip = signed_permission_slip.to_json

    assert_not signed_slip.save
    assert signed_slip.errors.any?
    assert_equal "B", signed_slip.errors.full_messages[0][0]
  end

  test "incorrect signature" do
    signed_slip, signed_permission_slip = get_slip_with_cert

    signed_permission_slip = get_full_permission_slip
    signed_slip.signed_permission_slip = signed_permission_slip.to_json

    assert_not signed_slip.save
    assert signed_slip.errors.any?
    assert_equal "C", signed_slip.errors.full_messages[0][0]
  end

  test "repeated cert subject with different cert" do
    cert_pem = File.read("certs/user_cert2.pem")
    us = User.find_or_create_by(certificate: cert_pem) 
    assert us.save

    cert_pem_rep = File.read("certs/user_cert2_repeated.pem")
    signed_slip, signed_permission_slip = get_slip_with_cert
    signed_permission_slip = get_full_permission_slip
    signed_permission_slip[:certificate] = cert_pem_rep

    privkey = OpenSSL::PKey::read(File.read("certs/keys/user2_repeated_private_key.pem"), "1234")
    digest = OpenSSL::Digest::SHA256.new
    signature = privkey.sign digest, signed_permission_slip[:slip].to_json
    signed_permission_slip[:signature] = Base64.encode64(signature)
    signed_slip.signed_permission_slip = signed_permission_slip.to_json

    assert_not signed_slip.save
    assert signed_slip.errors.any?
    assert_equal "D", signed_slip.errors.full_messages[0][0]
  end

  test "permissions not allowed" do
    cert_pem = File.read("certs/user_cert.pem")
    us = User.find_or_create_by(certificate: cert_pem) 
    assert us.save

    signed_slip = get_correct_signed_slip

    assert_not signed_slip.save
    assert signed_slip.errors.any?
    assert_equal "E", signed_slip.errors.full_messages[0][0]
  end

  test "user not in database" do
    signed_slip = get_correct_signed_slip

    assert_not signed_slip.save
    assert signed_slip.errors.any?
    assert_equal "F", signed_slip.errors.full_messages[0][0]
  end

  test "Incorrect permissions format" do
    cert_pem = File.read("certs/user_cert.pem")
    us = User.find_or_create_by(certificate: cert_pem)
    perm = Permission.find_or_create_by(perm: "read Article")
    us.permissions.push(perm) 
    assert us.save

    signed_slip, signed_permission_slip = get_slip_with_cert
    signed_permission_slip[:slip] ={}
    signed_permission_slip[:slip][:permissions] = ["any_non_two_words_string"]
    signed_permission_slip[:slip][:expiration_date] = Time.now + 60*60*24

    privkey = OpenSSL::PKey::read(File.read("certs/keys/user_private_key.pem"), "1234")
    digest = OpenSSL::Digest::SHA256.new
    signature = privkey.sign digest, signed_permission_slip[:slip].to_json
    signed_permission_slip[:signature] = Base64.encode64(signature)

    signed_slip.signed_permission_slip = signed_permission_slip.to_json
    assert_not signed_slip.save
    assert signed_slip.errors.any?
    assert_equal "G", signed_slip.errors.full_messages[0][0]
  end

  test "Everything correct" do
    cert_pem = File.read("certs/user_cert.pem")
    us = User.find_or_create_by(certificate: cert_pem)
    perm = Permission.find_or_create_by(perm: "read Article")
    us.permissions.push(perm) 
    perm = Permission.find_or_create_by(perm: "create Article")
    us.permissions.push(perm) 
    assert us.save

    signed_slip = get_correct_signed_slip

    assert signed_slip.save, signed_slip.errors.full_messages
    assert_not signed_slip.errors.any?
  end

end
