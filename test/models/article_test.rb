require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
   test "Article creation" do
     a = Article.new
     assert_not a.save
     a[:title] = "1234"
     a[:text] = "dsfdsfds"
     assert_not a.save
     a[:title] = "12345"
     assert a.save
   end
end
