ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  #fixtures :all

  # Add more helper methods to be used by all tests here...

      def get_correct_signed_slip 
    signed_slip, signed_permission_slip = get_slip_with_cert
    signed_permission_slip = get_full_permission_slip
    privkey = OpenSSL::PKey::read(File.read("certs/keys/user_private_key.pem"), "1234")
    digest = OpenSSL::Digest::SHA256.new
    signature = privkey.sign digest, signed_permission_slip[:slip].to_json
    signed_permission_slip[:signature] = Base64.encode64(signature)
    signed_slip.signed_permission_slip = signed_permission_slip.to_json
    return signed_slip
  end

  def get_slip_with_cert
    signed_slip = SignedSlip.new
    signed_slip.session_id = "any id"
    signed_permission_slip = {}
    signed_permission_slip[:certificate] = File.read("certs/user_cert.pem")
    return signed_slip, signed_permission_slip
  end

  def get_full_permission_slip
    signed_permission_slip = {}
    signed_permission_slip[:certificate] = File.read("certs/user_cert.pem")
    signed_permission_slip[:slip] = {}
    signed_permission_slip[:slip][:permissions] = []    
    signed_permission_slip[:slip][:permissions].push("create Article")  
    signed_permission_slip[:slip][:expiration_date] = Time.now + 60*60*24
    signed_permission_slip[:signature] = "an incorrect signature"
    return signed_permission_slip
  end

end
