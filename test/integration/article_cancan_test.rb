require 'test_helper'

class ArticleCancanTest < ActionDispatch::IntegrationTest

  #some auxiliar methods defined in test/test_helper.rb

  test "manage accepts everything but only gives access to what is in slip" do
    cert_pem = File.read("certs/user_cert.pem")
    us = User.find_or_create_by(certificate: cert_pem)
    perm = Permission.find_or_create_by(perm: "manage Article")
    us.permissions.push(perm) 

    signed_slip = get_correct_signed_slip #a slip with create Article permmission only

    assert signed_slip.save

    ability = Ability.new(signed_slip)
    assert ability.cannot?(:destroy, Article.new)
    assert ability.cannot?(:edit, Article.new)
    assert ability.can?(:create, Article.new)    
    assert ability.cannot?(:edit, Comment.new)
  end

  test "everyone can read" do #no analphabets here
    ability = Ability.new(SignedSlip.new)
    assert ability.can?(:read, Article.new)
    assert ability.can?(:read, Comment.new)
  end
end
